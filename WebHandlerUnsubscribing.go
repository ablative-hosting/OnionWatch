package main

//"github.com/btcsuite/go-socks/socks"
import (
	"database/sql"
	"fmt"
	//"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"html/template"
	"log"
	"net/http"
)

func WebUnsubscribeVerify(w http.ResponseWriter, r *http.Request, db *sql.DB, FQDN string) {
	if r.Method == "POST" {
		tmpl, err := template.New("success").ParseFiles("assets/templates/unsubscribe_verify.html")
		err = tmpl.Execute(w, nil)

		if err != nil {
			log.Fatal(err)
		}

	} else if r.Method == "GET" {

		tmpl, err := template.New("verify").ParseFiles("assets/templates/unsubscribe_verify.html")
		err = tmpl.Execute(w, nil)

		if err != nil {
			log.Fatal(err)
		}

	} else {
		//We don't accept PUT / DELETE etc on the /create/ URL
		w.Header().Set("ow-success", "false")
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Method %s is not allowed!", r.Method)
		fmt.Fprintf(w, "Method %s is not allowed!", r.Method)
	}

}

func WebUnsubscribe(w http.ResponseWriter, r *http.Request, db *sql.DB, FQDN string) {

	if r.Method == "POST" {

		r.ParseForm()
		payload := Unsubscribe{ContactEmail: r.FormValue("email")}
		emailErr := SendUnsubscribe(payload, db)

		if emailErr != nil {
			w.Header().Set("ow-success", "false")
			//TODO Write something out to the user (preferably with a template)
		}

		tmpl, err := template.New("success").ParseFiles("assets/templates/unsubscribe.html")

		err = tmpl.Execute(w, nil)

		if err != nil {
			log.Fatal(err)
		}
		return

	} else if r.Method == "GET" {

		tmpl, err := template.New("unsubscribe").ParseFiles("assets/templates/unsubscribe.html")
		err = tmpl.Execute(w, nil)

		if err != nil {
			log.Fatal(err)
		}

	} else {
		//We don't accept PUT / DELETE etc on the /create/ URL
		w.Header().Set("ow-success", "false")
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Method %s is not allowed!", r.Method)
		fmt.Fprintf(w, "Method %s is not allowed!", r.Method)
	}
}
