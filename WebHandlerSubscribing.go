package main

//"github.com/btcsuite/go-socks/socks"
import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"
)

func WebSubscribe(w http.ResponseWriter, r *http.Request, db *sql.DB, FQDN string) {

	if r.Method == "POST" {

		r.ParseForm()

		if r.FormValue("type") == "redirect" {
			if r.FormValue("onion_addr") != "" {
				w.Header().Add("Location", "/subscribe/hidden-service/"+r.FormValue("onion_addr"))
			} else {

				w.Header().Add("Location", "/subscribe/relay/"+r.FormValue("fingerprint"))
			}
			w.WriteHeader(http.StatusFound)
			fmt.Fprintf(w, "Redirecting you to the correct subscription location")
			return
		}

		subscribeErr := CreateSubscription(r, db)

		if subscribeErr == nil {
			tmpl, err := template.New("subscribe").ParseFiles("assets/templates/subscribe_success.html")
			err = tmpl.Execute(w, GenericMessage{Message: "Please check your email for a subscription confirmation"})

			if err != nil {
				log.Fatal(err)
			}
		} else {
			w.Header().Set("ow-success", "false")
			w.WriteHeader(http.StatusBadRequest)
			errOut := ErrorOutput{Error: subscribeErr.Error()}

			tmpl, err := template.New("error").ParseFiles("assets/templates/subscribe_error.html")
			err = tmpl.Execute(w, errOut)
			//fmt.Fprintf(w, "There was an error processing your subscription: %s", subscribeErr)
			if err != nil {
				log.Fatal(err)
			}
		}

		return

	} else if r.Method == "GET" {
		url := strings.Split(r.URL.String(), "/")
		log.Println(url[2])
		//log.Println(url[3])

		//Check whether we are monitoring a hidden service or a relay
		if strings.ToLower(url[2]) == "relay" {
			relayDetails, relayErr := FetchRelay(url[3])

			//We don't really care if fetching the relay info worked or not
			if relayErr != nil {
				log.Println(relayErr)
			} else {
				log.Println("Relay: " + relayDetails.NickName + " / " + relayDetails.Fingerprint)
			}

			tmpl, err := template.New("subscribe").ParseFiles("assets/templates/subscribe_relay.html")
			err = tmpl.Execute(w, relayDetails)

			if err != nil {
				log.Fatal(err)
			}
		} else if strings.ToLower(url[2]) == "hidden-service" {
			hs := HiddenService{HSAddr: url[3], ReachableMessage: "Unknown"}

			tmpl, err := template.New("subscribe").ParseFiles("assets/templates/subscribe_hiddenservice.html")
			err = tmpl.Execute(w, hs)

			if err != nil {
				log.Fatal(err)
			}

		} else {
			tmpl, err := template.New("error").ParseFiles("assets/templates/subscribe_error.html")
			err = tmpl.Execute(w, ErrorOutput{Error: "Unknown Monitor Type"})

			if err != nil {
				log.Fatal(err)
			}

		}
	} else {
		//We don't accept PUT / DELETE etc on the /create/ URL
		w.Header().Set("ow-success", "false")
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Method %s is not allowed!", r.Method)
		fmt.Fprintf(w, "Method %s is not allowed!", r.Method)
	}
}

func WebSubscribeVerify(w http.ResponseWriter, r *http.Request, db *sql.DB, FQDN string) {

	url := strings.Split(r.URL.String(), "/")
	log.Println(url[3])

	dbErr := verifyEmail(url[3], db)

	if dbErr == nil {
		tmpl, err := template.New("verified").ParseFiles("assets/templates/verify_success.html")
		err = tmpl.Execute(w, nil)

		if err != nil {
			log.Fatal(err)
		}
	} else {
		w.Header().Set("ow-success", "false")
		w.WriteHeader(http.StatusBadRequest)
		if dbErr == sql.ErrNoRows {
			tmpl, _ := template.New("norows").ParseFiles("assets/templates/verify_failed.html")
			tmpl.Execute(w, nil)
		} else {
			fmt.Fprintf(w, "There was an error (%s) processing your verification request", dbErr)
		}
	}

}
