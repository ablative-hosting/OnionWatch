all: test build-linux build-docker build-openbsd build-docker-comp

run: docker-comp

docker-comp: test build-docker build-docker-comp
	docker-compose up

test:
	env NO_EMAIL=true go test

build-linux:
	go build -race -ldflags "-extldflags '-static'" -o OnionWatch-linux

build-docker:
	env CGO_ENABLED=0 GOOS=linux go build -ldflags "-extldflags '-static'" -o OnionWatch-docker

build-openbsd:
	env GOOS=openbsd go build -ldflags "-extldflags '-static'" -o OnionWatch-openbsd

build-docker-comp:
	docker-compose build
	docker-compose pull

clean:
	rm OnionWatch-*
