package main

//"github.com/btcsuite/go-socks/socks"
import (
	"crypto/md5"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"errors"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/go-sql-driver/mysql"
	//_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
	"os"
	"strings"
)

func SendUnsubscribe(payload Unsubscribe, db *sql.DB) error {

	//We'll handle hidden services first
	Stmt, prepareErr := db.Prepare("select verify_hash,url from hs_watch where email = ?")

	if prepareErr != nil {
		log.Println(prepareErr.Error()) //TODO this probably needs better handling
	}

	rows, execErr := Stmt.Query(payload.ContactEmail)

	if execErr != nil {
		log.Println(execErr)
	}

	//We use this to make sure we actually have something to unsubscribe from
	rowCount := 0

	for rows.Next() {
		rowCount++
		var Hash, Watch string
		err := rows.Scan(&Hash, &Watch)
		if err != nil {
			log.Println(err.Error()) //TODO this probably needs better handling
			continue
		}
		payload.Hashes = append(payload.Hashes, WatchHash{Hash: Hash, Watch: Watch})
	}

	//Now we'll handle relays
	Stmt, prepareErr = db.Prepare("select verify_hash,fingerprint from relay_watch where email = ?")

	if prepareErr != nil {
		log.Println(prepareErr.Error()) //TODO this probably needs better handling
	}

	rows, execErr = Stmt.Query(payload.ContactEmail)

	if execErr != nil {
		log.Println(execErr)
	}

	for rows.Next() {
		rowCount++
		var Hash, Watch string
		err := rows.Scan(&Hash, &Watch)
		if err != nil {
			log.Println(err.Error()) // proper error handling instead of panic in your app
			continue
		}
		payload.Hashes = append(payload.Hashes, WatchHash{Hash: Hash, Watch: Watch})
	}

	if rowCount > 0 {
		emailErr := SendUnsubscribeEmail(payload)
		return emailErr
	}

	log.Println("Not sending an email because it wasn't in the DB")
	return nil

}
func verifyEmail(verifyHash string, db *sql.DB) error {
	hashSplit := strings.Split(verifyHash, "-")

	table := "hs_watch"

	if hashSplit[0] == "rw" {
		table = "relay_watch"
	}

	log.Printf("Preparing %s query for hash %s\n", table, verifyHash)
	Stmt, prepareError := db.Prepare("select id from " + table + " where verify_hash = ?")

	if prepareError != nil {
		log.Println(prepareError)
		return prepareError
	}

	log.Println("Executing")
	var id int
	//id, execErr := Stmt.Exec(verifyHash)
	execErr := Stmt.QueryRow(verifyHash).Scan(&id)

	if execErr != nil {
		return execErr
	}

	//log.Println(id)

	log.Printf("Preparing update query for hash %s\n", verifyHash)
	newHash := GetSHA256Hash(verifyHash + "-verified")
	Stmt, prepareError = db.Prepare("update " + table + " set email_verified = true, verify_hash = ? where verify_hash = ?")

	if prepareError != nil {
		log.Println(prepareError)
		return prepareError
	}

	log.Println("Executing")
	_, execErr = Stmt.Exec(newHash, verifyHash)

	if execErr != nil {
		return execErr
	}
	return nil
}

func CreateSubscription(r *http.Request, db *sql.DB) error {

	r.ParseForm()

	if r.FormValue("type") == "hs" {
		return createHSSubscription(r, db)
	} else {
		return createRelaySubscription(r, db)
	}

}

func createHSSubscription(r *http.Request, db *sql.DB) error {
	r.ParseForm()
	hs := HiddenService{HSAddr: r.FormValue("hiddenservice")}

	//Dedupe key
	//md5Hash := GetMD5Hash(hsWatch.HSDetails.HSAddr + hsWatch.ContactEmail)
	sha256Hash := "hw-" + GetSHA256Hash(r.FormValue("email")+r.FormValue("hiddenservice")+HASHSEED)

	hsWatch := HSWatch{ContactEmail: r.FormValue("email"),
		GPGKey:        r.FormValue("gpg"),
		EmailVerified: false,
		VerifyHash:    sha256Hash,
		HSDetails:     hs}

	mc := memcache.New(os.Getenv("MEMCACHE_HOST") + ":11211")
	memCacheErr := mc.Set(&memcache.Item{Key: sha256Hash, Value: []byte(sha256Hash), Expiration: 86400})

	if memCacheErr != nil {
		log.Println(memCacheErr)
	}

	dbErr := writeHSSubscription(hsWatch, db)

	if dbErr == nil {
		smtpErr := SendVerificationEmail(hsWatch)

		log.Printf("Verification Key/Hash: %s", sha256Hash)

		if smtpErr != nil {
			log.Println(smtpErr)
			//return smtpErr
			return nil
		} else {
			return nil
		}
	} else {
		if dbErr.(*mysql.MySQLError).Number == 1062 {
			return errors.New("A subscription for this hidden service already exists")
		} else {
			log.Println(dbErr)
			return dbErr
		}
	}
	return nil
}

func writeHSSubscription(hs HSWatch, db *sql.DB) error {
	log.Println("Preparing")
	Stmt, prepareError := db.Prepare("INSERT INTO hs_watch (email,url,gpg_key,email_verified,verify_hash) VALUES (?, ?, ?, ?, ?)")

	if prepareError != nil {
		log.Println(prepareError)
		return prepareError
	}

	log.Println("Executing")
	_, execErr := Stmt.Exec(hs.ContactEmail, hs.HSDetails.HSAddr, hs.GPGKey, false, hs.VerifyHash)

	if execErr != nil {
		return execErr
	}

	return nil
}

func createRelaySubscription(r *http.Request, db *sql.DB) error {

	r.ParseForm()

	//Dedupe key
	//md5Hash := GetMD5Hash(relayWatch.Fingerprint + relayWatch.ContactEmail)
	sha256Hash := "rw-" + GetSHA256Hash(r.FormValue("email")+r.FormValue("fingerprint")+HASHSEED)

	relayWatch := RelayWatch{Fingerprint: r.FormValue("fingerprint"),
		ContactEmail:  r.FormValue("email"),
		GPGKey:        r.FormValue("gpg"),
		EmailVerified: false,
		VerifyHash:    sha256Hash,
	}

	mc := memcache.New(os.Getenv("MEMCACHE_HOST") + ":11211")
	memCacheErr := mc.Set(&memcache.Item{Key: sha256Hash, Value: []byte(sha256Hash), Expiration: 86400})

	if memCacheErr != nil {
		log.Println(memCacheErr)
	}

	dbErr := writeRelaySubscription(relayWatch, db)

	if dbErr == nil {

		smtpErr := SendVerificationEmail(relayWatch)

		log.Printf("Verification Key/Hash: %s ", sha256Hash)

		if smtpErr != nil {
			log.Println(smtpErr)
			return smtpErr
		} else {
			return nil
		}
	} else {
		return dbErr
	}
}

func writeRelaySubscription(rs RelayWatch, db *sql.DB) error {
	log.Println("Preparing")
	Stmt, prepareError := db.Prepare("INSERT INTO relay_watch (email,fingerprint,gpg_key,email_verified,verify_hash) VALUES (?, ?, ?, ?, ?)")

	if prepareError != nil {
		log.Println(prepareError)
		return prepareError
	}

	log.Println("Executing")
	_, execErr := Stmt.Exec(rs.ContactEmail, rs.Fingerprint, rs.GPGKey, false, rs.VerifyHash)

	if execErr != nil {
		return execErr
	}

	return nil
}

func GetMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func GetSHA256Hash(text string) string {
	hash := sha256.New()
	hash.Write([]byte(text))
	md := hash.Sum(nil)
	return hex.EncodeToString(md)
}
