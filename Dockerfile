#ENV AUTHKEY | CONSULNAME | DB_USER | DB_PASS | LISTEN_IP | BTCHOST
FROM scratch
ADD https://curl.haxx.se/ca/cacert.pem /etc/ssl/certs/
COPY OnionWatch-docker /onionwatch
COPY assets /assets
EXPOSE 8090

ENTRYPOINT ["/onionwatch"]
