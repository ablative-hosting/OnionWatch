package main

import (
	"bytes"
	"crypto/tls"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"gopkg.in/gomail.v2"
	"log"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"
)

const (
	BRASSHORNCOMMSPUBLICKEY = `-----BEGIN GPG PUBLIC KEY BLOCK-----
Version: OpenGPG.js v3.1.2
Comment: https://openpgpjs.org

xsBNBFngcjQBCAC17uUc0x5o1y/iCavqduGLIfWMaFnMw3JCluqZSjdpU+XS
Klk7J51OwLYBBR93hBKmoe0MCJaJo4DL0ISxPPVD8QOVXfcDpmtoTDv3iyBe
GSchhf8xH9/+iHa/oejzm2DTEonvdc1WUC376yUoVg0FL5ksUqo9ti/wVjhH
4GdjNMF7KM6K0JpnA/xKIqoisuOquu6ueJuEfWBsPR1i+S8J3k9n4WUC2KdX
+d4ueOvncxhzaAXdQv8pLQosvch8SG+bgjDClxpyyO3BkRPU2uskh2h2Mh07
LdTGdSaRNnfXOR2AIDcIE/bd+gG0Pj2h1ekHut5KWg/xmwSjGy7477dxABEB
AAHNQ2hlbGxvQGJyYXNzaG9ybmNvbW11bmljYXRpb25zLnVrIDxoZWxsb0Bi
cmFzc2hvcm5jb21tdW5pY2F0aW9ucy51az7CwHUEEAEIACkFAlngcjQGCwkH
CAMCCRCRRB2lN9cdFgQVCAoCAxYCAQIZAQIbAwIeAQAAsC4IALEqFf9fZ6vC
m8+kzT4AKD859j8W06lhwQzhY43VZtUTrPqCPlx1Y7KhlcIZ5pdXXC7Zm72h
06j3m469w0ugs5TtfjX7BQQjQTV2y49kFhATH8DpvoB0R9ovW/Wj1w4rKZCs
ILgJC4oDJtDwVfyXACn8zZv6cV58znVdL5ANLAQH4h6AKrp0cr9vuovzHDDH
EEwk7f4WRcjwZgGffqQl3YyVuBaO52jYkHjJ3dkMzYYT2qAOLmVXu8bNQJkj
LtO1Ezc3iIWh2MKvDDwilYqgveR2Pxo/LCWDieCXgQzCxFc+VIICF6J0TbLh
t+SKkN05bTgI3UAeWkOOi8jDmBPdTsHOwE0EWeByNAEIAMETAtLtmCYdb5NK
PSeQn1oQJz2k3Y4Si7+eZWi2i7Gujv7Wmf+2uUKzVAunzZ6g7xe7HZLWD5Og
vo/sgalhJ4a8JdPSqio30rbBHVCuzgTzrvP2Kp4s8lPtNYX53YAC5/it8EiY
NDeneRKZ2HbYzTuOlOBBUg49WWwvAJ+iNu8a4MKZA7DDIpvZ0dhpS99hwHf3
n6RR+K8Ofezr/Jm06sfD+82l5YWOYsr+f0S7NgUhXXgOr/ePC0LFmiyLA2of
Z7yszbxQUjS0OFpPH95ollcukN0579QRAXFZd/KRgB0ZHbDw6mdgN4jI9AKG
v/5phayoWjoYtK+nLAmCUtjewI8AEQEAAcLAXwQYAQgAEwUCWeByNQkQkUQd
pTfXHRYCGwwAAMZ6CACy8PXZ9dZZxdaRpSmDIk8Fm06RqI3Vf3d7qg68BRXk
vOUpkboSIMKvUkwIsApQf5fAeAU/TKQ44DqrjJHx6QQdZxBk1mm0tKvFRQBm
iq2FnTa5u0rxhqECtlTWf4Pqr9B//3sWhUnlebLraSI6jg/QuyCY40Unhvc8
nGyrLuW26w0h9M5AkospRtEgc4mgv26zzHG92vFBW1/hekJX4Hq0vhrEgfcG
U3fsAzDZQ5Pd1cZB/9PIYGynIY/ldDPEvGoHqYxEM1kStIkBzJrxUtYPcnEO
0PX/pOqt1DmnWbFBIUwOi60if7lpF4CHNnpvF4QJulDHsdG1pEUBlA1AgCtU

=sioI
-----END GPG PUBLIC KEY BLOCK-----`

	VERIFICATION_TEMPLATE = `Hello,
This email addresses has been provisionally subscribed 
to alerts from OnionWatch.email.

Please click this link to confirm your subscription;
https://onionwatch.email/subscribe/verify/{{.VerifyHash}}/

Thank you.
OnionWatch EmailBot`

	UNSUBSCRIBE_TEMPLATE = `Hello,

Clicking any of the below links will allow you to unsubscribe from the alerts;

{{range $index, $element := .Hashes}}
{{.Watch}}
	https://onionwatch.email/unsubscribe/verify/{{.Hash}}/

{{end}}

Thank you.

OnionWatch EmailBot`

	RELAY_UP_NOTIFY_TEMPLATE = `Hello,

Relay https://metrics.torproject.org/rs.html#details/{{.Identifier}} is back up!

OnionWatch EmailBot`

	RELAY_DOWN_NOTIFY_TEMPLATE = `Hello,

Relay https://metrics.torproject.org/rs.html#details/{{.Identifier}} has gone down!

OnionWatch EmailBot`
)

func SendRelayChangeEmail(payload GenericNotification) error {

	var emailBody string
	var tmplErr error

	if payload.Up == true {
		emailBody, tmplErr = generateEmailTemplate(payload, RELAY_UP_NOTIFY_TEMPLATE)
	} else {
		emailBody, tmplErr = generateEmailTemplate(payload, RELAY_DOWN_NOTIFY_TEMPLATE)
	}

	if tmplErr != nil {
		emailBody = "A relay you are monitoring has changed state, unfortunately there was an issue processing the details"
	}

	var encStr string
	if payload.Key() != "" {
		var encErr error
		encStr, encErr = encryptUserPayload(emailBody, payload.Key())

		if encErr != nil {
			log.Println("There was an error encrypting the message to the user: ", encErr.Error())
			log.Println(payload.Key())
			encStr = emailBody
		}
	} else {
		encStr = emailBody
	}

	return EmailUser(encStr, "OnionWatch: Relay Up", payload.Email(), "")
}

func SendVerificationEmail(payload GenericWatch) error {
	emailBody, tmplErr := generateEmailTemplate(payload, VERIFICATION_TEMPLATE)

	if tmplErr != nil {
		emailBody = "Please click this link to verify your subscription: https://onionwatch.email/subscribe/verify/" + payload.Hash()
	}

	var encStr string
	if payload.Key() != "" {
		var encErr error
		encStr, encErr = encryptUserPayload(emailBody, payload.Key())

		if encErr != nil {
			log.Println("There was an error encrypting the message to the user: ", encErr.Error())
			log.Println(payload.Key())
			encStr = emailBody
		}
	} else {
		encStr = emailBody
	}

	return EmailUser(encStr, "OnionWatch Subscription Confirmation", payload.Email(), "")
}

func SendUnsubscribeEmail(payload GenericWatch) error {
	emailBody, tmplErr := generateEmailTemplate(payload, UNSUBSCRIBE_TEMPLATE)

	log.Println(payload)

	if tmplErr != nil {
		emailBody = "There was a problem generating your email"
		log.Println(tmplErr)
	}

	var encStr string
	if payload.Key() != "" {
		var encErr error
		encStr, encErr = encryptUserPayload(emailBody, payload.Key())

		if encErr != nil {
			log.Println("There was an error encrypting the message to the user: ", encErr.Error())
			encStr = emailBody
		}
	} else {
		encStr = emailBody
	}

	log.Println(encStr)

	return EmailUser(encStr, "OnionWatch Subscription Confirmation", payload.Email(), "")
}

func EmailUser(body, subject, address, errStr string) error {

	if os.Getenv("NO_EMAIL") == "true" {
		log.Println("Skipping email sending due to env vars")
		log.Printf("Would have sent\n\n To: %s\n Subject: %s\n Body: %s\n", address, subject, body)
		return nil
	}

	m := gomail.NewMessage()
	m.SetHeader("From", "noreply@onionwatch.email")
	m.SetHeader("To", address)
	m.SetHeader("Subject", subject)
	m.SetHeader("Message-Id", "ow+"+strconv.FormatInt(time.Now().Unix(), 10)+"@onionwatch.email")
	m.SetBody("text/plain", body)

	d := gomail.Dialer{Host: "mta01.brasshorncomms.uk", Port: 25}
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	if sendErr := d.DialAndSend(m); sendErr != nil {
		return sendErr
	}

	return nil
}

func generateEmailTemplate(payload GenericWatch, email_template string) (string, error) {
	var tpl bytes.Buffer

	tmpl, tmplErr := template.New("test").Parse(email_template)

	if tmplErr != nil {
		log.Println("Templating error: ", tmplErr.Error())
	}

	tmplErr = tmpl.Execute(&tpl, payload)

	if tmplErr == nil {
		return tpl.String(), nil
	}

	return "", tmplErr
}

func encryptUserPayload(template, pgpKey string) (string, error) {
	gpgPGP := strings.Replace(pgpKey, "GPG PUBLIC KEY BLOCK", "PGP PUBLIC KEY BLOCK", -1)
	keyBuffer := bytes.NewBufferString(gpgPGP)
	entityList, err := openpgp.ReadArmoredKeyRing(keyBuffer)

	if err == nil {

		buf := new(bytes.Buffer)
		w, encryptErr := openpgp.Encrypt(buf, entityList, nil, nil, nil)
		if encryptErr != nil {
			return "", encryptErr
		}
		_, writeErr := w.Write([]byte(template))
		if writeErr != nil {
			return "", writeErr
		}
		writeCloseErr := w.Close()
		if writeCloseErr != nil {
			return "", writeCloseErr
		}

		//Armour the output
		armourBuffer := new(bytes.Buffer)
		headers := make(map[string]string)
		headers["Version"] = "Ablative Hosting v1"

		encodeWriter, encodeErr := armor.Encode(armourBuffer, "PGP MESSAGE", headers)

		if encodeErr == nil {

			_, encodeWriteErr := encodeWriter.Write(buf.Bytes())
			encodeWriter.Close()

			if encodeWriteErr != nil {
				return "", encodeWriteErr
			}

			// Output encrypted/encoded string
			//log.Println("Encrypted Secret:", armourBuffer.String())

			return armourBuffer.String(), nil
		}

		return "", encodeErr
	}

	return "", err
}
