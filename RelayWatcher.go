package main

import (
	//"encoding/json"
	//"github.com/boltdb/bolt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	//  "math"
	"strconv"
	"strings"
	"time"
)

// RelayWatcher is the go routine that iterates over all the RelayWatcher objects in the DB and checks their status
// If the state has changed from the last run the user is emailed a notification (optionally GPG encrypted).
func RelayWatcher(db *sql.DB) {
	for {
		err := db.Ping()
		if err == nil {
			rows, queryErr := db.Query("select fingerprint,group_concat(email) from relay_watch where email_verified = true group by fingerprint")
			if queryErr != nil {
				log.Println(queryErr.Error())
			}

			for rows.Next() {
				var Addr, Emails string
				scanErr := rows.Scan(&Addr, &Emails)
				if scanErr != nil {
					log.Println(scanErr.Error())
				}

				log.Printf("Searching for %s to tell %s", Addr, Emails)
				relay, fetchErr := FetchRelay(Addr)

				if fetchErr == nil {
					// Check the relay compared to our last check
					checkErr, changed := CheckRelayState(relay, db)

					if checkErr != nil {
						log.Println(checkErr)
					}

					if changed == true {
						log.Printf("%s has changed, currently", relay.NickName)

						notifyErr := NotifyUser(RelayStateTemplate{}, relay, Emails, db)

						if notifyErr != nil {
							log.Println(notifyErr)
						}
					} else {
						log.Printf("%s has not changed", relay.NickName)
					}
				} else {
					log.Println(fetchErr)
				}
			}
		} else {
			log.Println(err.Error())
		}

		checkTime := time.Now().Unix()
		log.Println("RelayWatch check time " + strconv.FormatInt(checkTime, 10))
		//time.Sleep(300000 * time.Millisecond)
		time.Sleep(30 * time.Second)
	}
}

func CheckRelayState(queriedRelay Relay, db *sql.DB) (error, bool) {
	log.Println("--------------------- CheckRelayState")
	//if nil rows
	var storedRelay Relay

	Stmt, prepareError := db.Prepare("select fingerprint, name, or_address, dir_address, last_seen, first_seen, running, flags, asn, as_name, contact from relay where fingerprint = ?")

	if prepareError != nil {
		log.Println(prepareError)
		return prepareError, false
	}

	var OrAddresses, Flags string

	execErr := Stmt.QueryRow(queriedRelay.Fingerprint).Scan(&storedRelay.Fingerprint,
		&storedRelay.NickName,
		&OrAddresses,
		&storedRelay.DirAddress,
		&storedRelay.LastSeen,
		&storedRelay.FirstSeen,
		&storedRelay.Running,
		&Flags,
		&storedRelay.ASNumber,
		&storedRelay.ASName,
		&storedRelay.Contact,
	)

	if execErr == sql.ErrNoRows {
		StoreRelayState(queriedRelay, db)
		return nil, false
	}

	if execErr != nil {
		return execErr, false
	}

	//Whether its changed or not update our cache
	updateErr := UpdateRelayState(queriedRelay, db)

	if updateErr != nil {
		log.Println(updateErr)
	}

	//In the future we might email if things change, for now it's just up or down
	if queriedRelay.Running == storedRelay.Running {
		return nil, false
	} else {
		return nil, true
	}
}

func UpdateRelayState(relay Relay, db *sql.DB) error {
	log.Println("--------------------- UpdateRelayState")
	Stmt, prepareError := db.Prepare("UPDATE relay SET name = ?, or_address = ?, dir_address = ?, last_seen = ?, first_seen = ?, running =?, flags = ?, asn = ?, as_name = ?, contact = ? where fingerprint = ?")

	if prepareError != nil {
		log.Println("------------ PREPARE")
		log.Println(prepareError)
		return prepareError
	}

	log.Println("Executing")
	var ORAddresses, Flags string
	for _, orRecord := range relay.OrAddresses {
		ORAddresses += orRecord + ","
	}

	for _, flagRecord := range relay.Flags {
		Flags += flagRecord + ","
	}

	_, execErr := Stmt.Exec(relay.NickName,
		strings.Trim(ORAddresses, ","),
		relay.DirAddress,
		relay.LastSeen,
		relay.FirstSeen,
		relay.Running,
		strings.Trim(Flags, ","),
		relay.ASNumber,
		relay.ASName,
		relay.Contact,
		relay.Fingerprint,
	)

	if execErr != nil {
		log.Printf("UpdateRelayState Error: %s\n", execErr)
		return execErr
	}

	return nil
}

func StoreRelayState(relay Relay, db *sql.DB) error {
	log.Println("----------------------------- StoreRelayState")
	Stmt, prepareError := db.Prepare("INSERT INTO relay (fingerprint, name, or_address, dir_address, last_seen, first_seen, running, flags, asn, as_name, contact) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

	if prepareError != nil {
		log.Println(prepareError)
		return prepareError
	}

	log.Println("Executing")
	log.Println(relay)
	var ORAddresses, Flags string
	for _, orRecord := range relay.OrAddresses {
		ORAddresses += orRecord + ","
	}

	for _, flagRecord := range relay.Flags {
		Flags += flagRecord + ","
	}

	_, execErr := Stmt.Exec(relay.Fingerprint,
		relay.NickName,
		strings.Trim(ORAddresses, ","),
		relay.DirAddress,
		relay.LastSeen,
		relay.FirstSeen,
		relay.Running,
		strings.Trim(Flags, ","),
		relay.ASNumber,
		relay.ASName,
		relay.Contact,
	)

	if execErr != nil {
		log.Println(execErr)
		return execErr
	}

	return nil
}

func NotifyUser(whatsChanged RelayStateTemplate, relay Relay, emails string, db *sql.DB) error {

	//Split the emails
	emailArray := strings.Split(emails, ",")

	for _, email := range emailArray {

		Stmt, prepareError := db.Prepare("select fingerprint, gpg_key from relay_watch where fingerprint = ? and email = ?")

		if prepareError != nil {
			log.Println(prepareError)
			return prepareError
		}

		notification := GenericNotification{ContactEmail: email,
			Up: relay.Running,
		}

		execErr := Stmt.QueryRow(relay.Fingerprint, strings.TrimSpace(email)).Scan(&notification.Identifier,
			&notification.GPGKey,
		)

		if execErr == sql.ErrNoRows {
			log.Printf("There was an issue matching a row for %s and %s\n", relay.Fingerprint, strings.TrimSpace(email))
			continue
		}

		if execErr == nil {
			emailErr := SendRelayChangeEmail(notification)

			if emailErr != nil {
				log.Println(emailErr)
			}
		}

	}

	return nil
}
