package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	HSTSEXPIRY  = 94670856
	APIVERSION  = 1
	HSTSENABLED = false
	HASHSEED    = "3qy5r0wierfhjwiejisdh0whrt8wh08rh0swdhf0ss0f0sdfhsd0fhsdf8h0d"
	FROM_EMAIL  = ""
	SMTP_HOST   = "localhost:25"
)

func main() {
	log.Println("---------------------------------------------")

	var conf CoreConf

	//conf.DbPath = os.Getenv("DBPATH")
	conf.DBHost = os.Getenv("DB_HOST")
	conf.DBUser = os.Getenv("DB_USER")
	conf.DBPass = os.Getenv("DB_PASS")
	conf.ListenIP = os.Getenv("LISTENIP")
	conf.ListenIPv6 = os.Getenv("LISTENIPV6")
	//conf.ListenPort = os.Getenv("LISTENPORT")
	conf.ListenPort = 8090
	conf.SOCKSConfig = os.Getenv("SOCKSCONFIG")
	conf.FQDN = os.Getenv("FQDN")

	if conf.FQDN == "" {
		conf.FQDN = "onionwatch.email"
	}

	if conf.ListenIP == "" {
		conf.ListenIP = "0.0.0.0"
	}

	if conf.SOCKSConfig == "" {
		conf.SOCKSConfig = "127.0.0.1:9050"
	}

	//-------------------------------------------------------------------------------------
	//Prepare MySQL
	if conf.DBHost == "" {
		conf.DBHost = "db01.ablative.hosting"
	}

	log.Printf("Using %s as the DB server.\n", conf.DBHost)

	log.Println(conf.DBUser + " + " + conf.DBPass)

	db, dbErr := sql.Open("mysql", conf.DBUser+":"+conf.DBPass+"@tcp("+conf.DBHost+":3306)/onionwatch")

	if dbErr != nil {
		//statsdclient.Incr("dberr", 1)
		log.Fatal(dbErr)
	}

	//TODO this properly!
	err := db.Ping()
	if err != nil {
		log.Println(err.Error())
	}

	//Start our watcher process
	go RelayWatcher(db)
	go HSWatcher(db)

	//Handle static web pages
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { WebIndex(w, r) })
	http.HandleFunc("/about/", func(w http.ResponseWriter, r *http.Request) { WebAbout(w, r) })

	//Handle functionality
	http.HandleFunc("/manage/", func(w http.ResponseWriter, r *http.Request) { WebManage(w, r, db, conf.FQDN) })
	http.HandleFunc("/subscribe/verify/", func(w http.ResponseWriter, r *http.Request) { WebSubscribeVerify(w, r, db, conf.FQDN) })
	http.HandleFunc("/subscribe/", func(w http.ResponseWriter, r *http.Request) { WebSubscribe(w, r, db, conf.FQDN) })
	//http.HandleFunc("/subscribe/relay/", func(w http.ResponseWriter, r *http.Request) { WebSubscribeRelay(w, r, db, conf.FQDN) })
	//http.HandleFunc("/subscribe/hidden-service/", func(w http.ResponseWriter, r *http.Request) { WebSubscribeHS(w, r, db, conf.FQDN) })
	http.HandleFunc("/unsubscribe/verify/", func(w http.ResponseWriter, r *http.Request) { WebUnsubscribeVerify(w, r, db, conf.FQDN) })
	http.HandleFunc("/unsubscribe/", func(w http.ResponseWriter, r *http.Request) { WebUnsubscribe(w, r, db, conf.FQDN) })

	//Serve up static resources
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("assets/css/"))))
	http.Handle("/fonts/", http.StripPrefix("/fonts/", http.FileServer(http.Dir("assets/fonts/"))))
	//http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("assets/js/"))))
	http.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir("assets/img/"))))

	ListenPort := strconv.FormatInt(conf.ListenPort, 10)
	if conf.TLS {
		http.ListenAndServeTLS(":"+ListenPort, conf.TLSCert, conf.TLSKey, nil)
	} else {
		http.ListenAndServe(":"+ListenPort, nil)
	}
}
