package main

import (
	"testing"
)

/*var hsPayloads = []struct {
	Payload MemberSignup
	Success bool
}{
	{GenericWatch{"UserName", "UserEmail", "RealName", "PGPKey", "SSHKey", "ErrorMeta"}, false},
	{MemberSignup{"root", "root@localhost", "Bob", BRASSHORNCOMMSPUBLICKEY, TESTSSH, ""}, true},
	{MemberSignup{"", "", "", "", "", ""}, false},
	{MemberSignup{"", "", "", TESTPGP, "", "Error String"}, true},
}*/

var hsSignup = []struct {
	Payload GenericWatch
	Success bool
}{
	{RelayWatch{VerifyHash: "", ContactEmail: "", GPGKey: ""}, false},
	{RelayWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "", GPGKey: ""}, false},
	{RelayWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "support@ablative.hosting", GPGKey: ""}, false},
	{RelayWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "support@ablative.hosting", GPGKey: TESTPGP}, true},
	{RelayWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "support@ablative.hosting", GPGKey: BRASSHORNCOMMSPUBLICKEY}, true},
	{HSWatch{VerifyHash: "", ContactEmail: "", GPGKey: ""}, false},
	{HSWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "", GPGKey: ""}, false},
	{HSWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "support@ablative.hosting", GPGKey: ""}, false},
	{HSWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "support@ablative.hosting", GPGKey: TESTPGP}, true},
	{HSWatch{VerifyHash: "THISISNOTREALLYAHASH", ContactEmail: "support@ablative.hosting", GPGKey: BRASSHORNCOMMSPUBLICKEY}, true},
}

func TestUserCrypto(t *testing.T) {
	t.Log(" ~~~~~~~~~~~\n\nTESTING USER CRYPTO\n\n~~~~~~~~~~~")
	for test, tt := range hsSignup {

		t.Log("------------------------------------------------------------------------")
		t.Log("Test: ", test)

		emailBody, tmplErr := generateEmailTemplate(tt.Payload, VERIFICATION_TEMPLATE)

		if tmplErr != nil {
			t.Errorf(tmplErr.Error())
		}

		encStr, err := encryptUserPayload(emailBody, tt.Payload.Key())

		t.Log("Template:\n", emailBody)

		t.Log("Resulting CryptoString:\n", encStr)

		//Some tests are *supposed* to return an error
		if err != nil {
			//Was this test supposed to be successful despite erroring?
			if tt.Success == true {
				t.Errorf(err.Error())
			}

			t.Log("(This test fails on purpose)")
		}
	}
}

func TestFullPath(t *testing.T) {
	t.Log(" ~~~~~~~~~~~\n\nTESTING interface{}\n\n~~~~~~~~~~~")
	for test, tt := range hsSignup {
		t.Log("------------------------------------------------------------------------")
		t.Log("Test: ", test)
		err := SendVerificationEmail(tt.Payload)

		//Some tests are *supposed* to return an error
		if err != nil {
			//Was this test supposed to be successful despite erroring?
			if tt.Success == true {
				t.Errorf(err.Error())
			}

			t.Log("(This test fails on purpose)")
		}

	}
}

var TESTPGP = `-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v3.1.2
Comment: https://openpgpjs.org

xsBNBFr7+d8BCADHEROFLD7yxbDhZYZVqS5f4j63oZNEwounppNq59vuXOQ1
ntJvJQWpBgrdvTzp6hickBxRyOVi5QH9+y+Dsdp5KYMHay5ZVbhSgKWdK05l
HCbT3tTdJ+IA7vnx1wE8XtquNHliLkFTyQMWFDdMp2Sr29lD5lRtWaEBcoAG
2c73ao1fgWxzXMk5O7PBmBDjAzC1CInQqKRh8kV58YdPl2+lgHlCOgb2ET5J
eyRFaXVk1Bs+vrkw94NwcbBbYn9r+R+6UIUfXu/NyAX60QvExSpoOZ74JrqB
O2i5iZO53SR3Iwx4A7GdlquKNQ6zZrHyKz2GTrk9ya0cTG7P5qvxAd2JABEB
AAHNSXN0YW5kYnlAbmV0d29ya3NhcmVtYWRlb2ZzdHJpbmcuY29tIDxzdGFu
ZGJ5QG5ldHdvcmtzYXJlbWFkZW9mc3RyaW5nLmNvbT7CwHUEEAEIACkFAlr7
+d8GCwkHCAMCCRAFgBVna81EVAQVCAoCAxYCAQIZAQIbAwIeAQAALqsIAL+d
KZec2KaSHNZnyI6JwXGormIl8H+N0hqqNIC73GUCFQSYbUQqaIETdilOOO6F
aqPHbfsvkFcRhz+NydXYjjOrj+mc0+lSnDmtOTxHAs7vZdMXerRd7ZPL8taQ
aus74xNkqokMfVesFPSv53wGwGHSK911Op7QH8UtNRkieB4j7kg7HPjPr7zK
wynPWMcFieD6vMcUPJSo46NefWIlooqTqGX44a9eLdPjQZUMYlXZMK1+T5kE
zYND9uGu/W0kihdfJZZ6xRcHZLo7bzvwRPqhiTwCnO299AVV4kcn3ItQGFwT
CWUhTI/n4UoEJQ1ir4l8/WjZaIuqWTIcl2EbPwHOwE0EWvv53wEIAL1bi/R3
In++qxQ8bLVn9fZ4GAfNsamrQ+SS6cDQCNQKpSX65jem6Yr66h/pOnl5Iryt
XSqozhkrxMDYpTxkHmJOGXCSaDJQAiIDbLhtbMtXZLYgtQ18XmRTck3t5jLU
GAAQo7jYL0UUowCs9cxckZX+Qb6N/CDfI/6bVHY3jSbjBAWhwXw3ExlH5sxP
LHvjKoGoM6MiIz/PcghC8BKsGFW1SUgwSu98P23t+FHgFtYhjpEpSloVxfW4
GxECqXWTaZNxL5qw/F6ordwgjSusjooRfX3Z9rhVKs9DnDbs8yXX2SXOWjE5
lmDK6zYqOMqZxVYsqj9CaaEfZ2HW3t3K7UkAEQEAAcLAXwQYAQgAEwUCWvv5
5wkQBYAVZ2vNRFQCGwwAAHaTCACsdEpPmQ8YZGDTWIf+IlgbI/4xfiZXtAtk
788VV2sQKykIzYnY+UNxj/xt5F42WGuRJQa9ng4/aVFk0YbbOINvNFV9TjM6
P7ttmdg0Z1xj9DeYupzwcS/3+9EbAbFWYeryJgZjldU0T2fU9E1WjoSZIAGZ
7kH9zoRnBVkFqm+hzARwfwAcEuclUJM68ppoG1mzDkVgc6kZvOh9UNku+Ypl
t/Qsa0TNxHznS/gBCsARRJ2t4T8OXVxibcj81EfiLyU7MWWxLWW6EquAgtda
KDSg4cLFfadm6J5itSwbDm6y77EuPwBBSbVC5JLFhqVP2B9IRFnMHPTiF6f5
cBgGLI/G
=DKv7
-----END PGP PUBLIC KEY BLOCK-----`
