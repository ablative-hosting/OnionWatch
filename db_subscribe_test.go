package main

import (
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"strings"
	"testing"
)

var dbRelays = []struct {
	Payload RelayWatch
	Success bool
}{
	{RelayWatch{Fingerprint: "", ContactEmail: "", GPGKey: ""}, false},
	{RelayWatch{Fingerprint: "THISISNOTREALLYAHASH", ContactEmail: "", GPGKey: ""}, false},
	{RelayWatch{Fingerprint: "THISISNOTREALLYAHASH", ContactEmail: "test@test.com", GPGKey: ""}, true},
	{RelayWatch{Fingerprint: "THISISNOTREALLYAHASH", ContactEmail: "test@test.com", GPGKey: TESTPGP}, true},
	{RelayWatch{Fingerprint: "THISISNOTREALLYAHASH", ContactEmail: "test@test.com", GPGKey: TESTPGP, VerifyHash: "AHash"}, true},
}

type Verify struct {
	Hash string
	ID   int
}

var verifyHashes = []struct {
	Payload Verify
	Success bool
}{
	{Verify{"rs-459jfgposjfi0349tr39tj0er", 0}, true},
	{Verify{"hs-45j034598e0tj0erut0er0ue", 1}, true},
}

func TestRelaySignup(t *testing.T) {
	t.Log(" ~~~~~~~~~~~\n\nTESTING RELAY SIGNUP\n\n~~~~~~~~~~~")
	for test, tt := range dbRelays {

		t.Log("------------------------------------------------------------------------")
		t.Log("Test: ", test)
		db, mock, err := sqlmock.New()
		if err != nil {
			t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
		}
		defer db.Close()

		mock.ExpectPrepare("INSERT INTO relay_watch \\(email,fingerprint,gpg_key,email_verified,verify_hash\\) VALUES ")

		mock.ExpectExec("INSERT INTO relay_watch \\(email,fingerprint,gpg_key,email_verified,verify_hash\\) VALUES \\(\\?, \\?, \\?, \\?, \\?\\)").WithArgs(tt.Payload.ContactEmail, tt.Payload.Fingerprint, tt.Payload.GPGKey, false, tt.Payload.VerifyHash).WillReturnResult(sqlmock.NewResult(1, 1))

		sqlErr := writeRelaySubscription(tt.Payload, db)

		//Some tests are *supposed* to return an error
		if sqlErr != nil {
			//Was this test supposed to be successful despite erroring?
			if tt.Success == true {
				t.Errorf(sqlErr.Error())
			} else {
				t.Log("(This test fails on purpose)")
			}
		}
	}
}

func TestVerify(t *testing.T) {
	t.Log(" ~~~~~~~~~~~\n\nTESTING EMAIL VERIFICATION\n\n~~~~~~~~~~~")
	for test, tt := range verifyHashes {

		t.Log("------------------------------------------------------------------------")
		t.Log("Test: ", test)
		db, mock, err := sqlmock.New()
		if err != nil {
			t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
		}
		defer db.Close()

		rows := sqlmock.NewRows([]string{"id"}).AddRow(tt.Payload.ID)
		hashSplit := strings.Split(tt.Payload.Hash, "-")

		table := "hs_watch"

		if hashSplit[0] == "rw" {
			table = "relay_watch"
		}

		t.Logf("Testting %s for %s\n", table, tt.Payload.Hash)
		mock.ExpectPrepare("select id from " + table + " where verify_hash = ?")

		mock.ExpectQuery("select id from " + table + " where verify_hash = \\?").WithArgs(tt.Payload.Hash).WillReturnRows(rows)

		mock.ExpectPrepare("update " + table + " set email_verified = true, verify_hash = \\? where verify_hash = \\?")

		newHash := GetSHA256Hash(tt.Payload.Hash + "-verified")
		mock.ExpectExec("update "+table+" set email_verified = true, verify_hash = \\? where verify_hash = \\?").WithArgs(newHash, tt.Payload.Hash).WillReturnResult(sqlmock.NewResult(1, 1))
		sqlErr := verifyEmail(tt.Payload.Hash, db)

		//Some tests are *supposed* to return an error
		if sqlErr != nil {
			//Was this test supposed to be successful despite erroring?
			if tt.Success == true {
				t.Errorf(sqlErr.Error())
			} else {
				t.Log("(This test fails on purpose)")
			}
		}
	}
}

var TESTPGP1 = `-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v3.1.2
Comment: https://openpgpjs.org

xsBNBFr7+d8BCADHEROFLD7yxbDhZYZVqS5f4j63oZNEwounppNq59vuXOQ1
ntJvJQWpBgrdvTzp6hickBxRyOVi5QH9+y+Dsdp5KYMHay5ZVbhSgKWdK05l
HCbT3tTdJ+IA7vnx1wE8XtquNHliLkFTyQMWFDdMp2Sr29lD5lRtWaEBcoAG
2c73ao1fgWxzXMk5O7PBmBDjAzC1CInQqKRh8kV58YdPl2+lgHlCOgb2ET5J
eyRFaXVk1Bs+vrkw94NwcbBbYn9r+R+6UIUfXu/NyAX60QvExSpoOZ74JrqB
O2i5iZO53SR3Iwx4A7GdlquKNQ6zZrHyKz2GTrk9ya0cTG7P5qvxAd2JABEB
AAHNSXN0YW5kYnlAbmV0d29ya3NhcmVtYWRlb2ZzdHJpbmcuY29tIDxzdGFu
ZGJ5QG5ldHdvcmtzYXJlbWFkZW9mc3RyaW5nLmNvbT7CwHUEEAEIACkFAlr7
+d8GCwkHCAMCCRAFgBVna81EVAQVCAoCAxYCAQIZAQIbAwIeAQAALqsIAL+d
KZec2KaSHNZnyI6JwXGormIl8H+N0hqqNIC73GUCFQSYbUQqaIETdilOOO6F
aqPHbfsvkFcRhz+NydXYjjOrj+mc0+lSnDmtOTxHAs7vZdMXerRd7ZPL8taQ
aus74xNkqokMfVesFPSv53wGwGHSK911Op7QH8UtNRkieB4j7kg7HPjPr7zK
wynPWMcFieD6vMcUPJSo46NefWIlooqTqGX44a9eLdPjQZUMYlXZMK1+T5kE
zYND9uGu/W0kihdfJZZ6xRcHZLo7bzvwRPqhiTwCnO299AVV4kcn3ItQGFwT
CWUhTI/n4UoEJQ1ir4l8/WjZaIuqWTIcl2EbPwHOwE0EWvv53wEIAL1bi/R3
In++qxQ8bLVn9fZ4GAfNsamrQ+SS6cDQCNQKpSX65jem6Yr66h/pOnl5Iryt
XSqozhkrxMDYpTxkHmJOGXCSaDJQAiIDbLhtbMtXZLYgtQ18XmRTck3t5jLU
GAAQo7jYL0UUowCs9cxckZX+Qb6N/CDfI/6bVHY3jSbjBAWhwXw3ExlH5sxP
LHvjKoGoM6MiIz/PcghC8BKsGFW1SUgwSu98P23t+FHgFtYhjpEpSloVxfW4
GxECqXWTaZNxL5qw/F6ordwgjSusjooRfX3Z9rhVKs9DnDbs8yXX2SXOWjE5
lmDK6zYqOMqZxVYsqj9CaaEfZ2HW3t3K7UkAEQEAAcLAXwQYAQgAEwUCWvv5
5wkQBYAVZ2vNRFQCGwwAAHaTCACsdEpPmQ8YZGDTWIf+IlgbI/4xfiZXtAtk
788VV2sQKykIzYnY+UNxj/xt5F42WGuRJQa9ng4/aVFk0YbbOINvNFV9TjM6
P7ttmdg0Z1xj9DeYupzwcS/3+9EbAbFWYeryJgZjldU0T2fU9E1WjoSZIAGZ
7kH9zoRnBVkFqm+hzARwfwAcEuclUJM68ppoG1mzDkVgc6kZvOh9UNku+Ypl
t/Qsa0TNxHznS/gBCsARRJ2t4T8OXVxibcj81EfiLyU7MWWxLWW6EquAgtda
KDSg4cLFfadm6J5itSwbDm6y77EuPwBBSbVC5JLFhqVP2B9IRFnMHPTiF6f5
cBgGLI/G
=DKv7
-----END PGP PUBLIC KEY BLOCK-----`
