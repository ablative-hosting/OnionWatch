package main

import (
	//"encoding/json"
	//"github.com/boltdb/bolt"
	"database/sql"
	"github.com/btcsuite/go-socks/socks"
	_ "github.com/go-sql-driver/mysql"
	//"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"strconv"
	"time"
)

func HSWatcher(db *sql.DB) {
	for {
		checkTime := time.Now().Unix()
		log.Println("HS | Hidden Service Check Time " + strconv.FormatInt(checkTime, 10))
		err := db.Ping()
		if err == nil {
			rows, queryErr := db.Query("select url,group_concat(email) from hs_watch where email_verified = true group by url")
			if queryErr != nil {
				log.Println(queryErr.Error()) // proper error handling instead of panic in your app
			}

			for rows.Next() {
				var HSAddr, Emails string
				scanErr := rows.Scan(&HSAddr, &Emails)
				if scanErr != nil {
					log.Println(scanErr.Error()) // proper error handling instead of panic in your app
				}

				log.Printf("Searching for %s to tell %s", HSAddr, Emails)
				up, fetchErr := FetchHS(HSAddr)

				if fetchErr == nil {
					if up {
						log.Printf("%s is up!", HSAddr)
					} else {
						log.Printf("%s is down!", HSAddr)
					}
				} else {
					log.Println(fetchErr)
				}
			}
		} else {
			log.Println(err.Error())
		}

		//time.Sleep(300000 * time.Millisecond)
		time.Sleep(30 * time.Second)
	}
}

func NotifyHSWatcher(email, gpgkey, hs string, isUp bool) error {

	to := []string{email}
	msg := []byte("To: " + email + "\r\n" +
		"Subject: RelayWatch Subscription for New Hidden Service\r\n" +
		"Hello,\r\n\r\n" +
		"Hidden service " + hs + " has gone down" +
		"\r\n")

	smtpErr := smtp.SendMail(SMTP_HOST, nil, "OnionWatch <notification@onionwatch.email>", to, msg)

	return smtpErr
}

func FetchHS(url string) (bool, error) {
	var client http.Client

	//proxy := &socks.Proxy{"127.0.0.1:9050", "", "", true}
	proxy := &socks.Proxy{"172.18.0.1:9050", "", "", true}
	tr := &http.Transport{
		Dial: proxy.Dial,
	}
	client = http.Client{
		Transport: tr,
		Timeout:   time.Duration(5 * time.Second),
	}

	req, httpReqErr := http.NewRequest("GET", "http://"+url, nil)
	if httpReqErr != nil {

		return false, httpReqErr
	}

	req.Header.Set("User-Agent", "OnionWatch.email/1.0 (+https://onionwatch.email/bot/)")

	resp, httpErr := client.Do(req)
	if httpErr != nil {
		return false, httpErr
	}

	defer resp.Body.Close()

	//body, _ := ioutil.ReadAll(resp.Body)
	//log.Println(string(body))

	if resp.StatusCode == 200 {
		return true, nil
	} else {
		return false, nil
	}
}
