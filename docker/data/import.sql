create table relay_watch (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
	email TEXT,
	fingerprint TEXT,
	gpg_key TEXT,
	password BINARY(60),
	verify_hash TEXT,
	email_verified BOOLEAN,
	last_checked DATETIME DEFAULT CURRENT_TIMESTAMP,
	last_seen DATETIME DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT U_EMAIL UNIQUE (fingerprint(40),email(128))
);

create table hs_watch (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
	email TEXT,
	url TEXT,
	gpg_key TEXT,
	password BINARY(60),
	verify_hash TEXT,
	email_verified BOOLEAN,
	last_checked DATETIME DEFAULT CURRENT_TIMESTAMP,
	last_seen DATETIME DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT U_EMAIL UNIQUE (url(64),email(128))

);

create table relay (
	fingerprint TEXT,
	name TEXT,
	or_address TEXT,
	dir_address TEXT,
	last_seen DATETIME,
	first_seen DATETIME,
	running BOOLEAN,
	flags TEXT,
	asn TEXT,
	as_name TEXT,
	contact TEXT,
	PRIMARY KEY (fingerprint(40)),
  	UNIQUE KEY fingerprint (fingerprint(40))
);

create table hs (
	url TEXT,
	reachable BOOLEAN
);
